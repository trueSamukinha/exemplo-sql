drop database SC;
create database SC;
use SC;

create table reciboEmpresa (
id2 int primary key not null,
Nome varchar(20) not null,
CPF varchar(15) not null,
Telefone varchar(20) not null,
AlturaPlaca int not null,
LarguraPlaca int not null,
Frase varchar(110) not null,
CorPlaca varchar(20) not null,
CorFrase varchar(20) not null,
ValorPlaca1 int,
situacao varchar(30),
valor_Sinal decimal(4,2)
);

create table caderninho(
  id int primary key not null,
  Nome varchar(20) not null,
  Telefone varchar(20) not null,
  AlturaPlaca int not null,
  LarguraPlaca int not null,
  Frase varchar(110),
  CorPlaca varchar(20),
  CorFrase varchar(20),
  area_1 decimal(10,2),
  Valor_Sinal int
);

create table cadastro(
  cpf varchar(20) primary key not null,
  nome varchar(64) not null,
  telefone varchar(64) not null,
  id integer,
  id_recibo integer,
  CONSTRAINT cadastro_id FOREIGN KEY (id) REFERENCES caderninho(id)
);

create table calculos(
  id1 int,
  area_1 decimal(10,2),
  CustoMaterial decimal(10,2),
  CustoDesenho decimal(10,2),
  ValorPlaca decimal(10,2),
  situação varchar(30)
);

insert into calculos values
(1, null, null, null, null, "Enviado!"),
(2, null, null, null, null, "Em preparo"),
(3, null, null, null, null, "Aguardando pagamento"),
(4, null, null, null, null, "Enviado!"),
(5, null, null, null, null, "Esperando confirmação"),
(6, null, null, null, null, "Aguardando pagamento");

insert into reciboEmpresa values
(1, "Samukinha", "264.804.737-41", "+55 31 99987-2719", 20, 20, "Nada é impossivel pra quem desiste", "Branca", "Verde", null, "Enviado!", null),

(2, "Vigarista", "123.45.789-00", "+55 11 98618-8845", 35, 40, "Nada é tão horrivel que não possa piorar!", "Cinza", "Preto", null, "Em preparo", null),

(3, "Bolsonaro", "865.971.623-57", "+55 18 99472-6533", 10, 10, "Talkey", "Branco", "Vermelho",null, "Aguardando pagamento", null),

(4, "Zeca PaGodinho", "512.741.202-96", "+55 41 98126-7410", 10, 20, "Seja o protagonista do seu fracasso", "Cinza", "Azul", null, "Enviado!", null),

(5, "Mocreia", "789.210.374-30", "+55 08 99161-9906", 100, 100, "O lado bom de ser feio é que sua namorada te ama de verdade agora o lado ruim é que você não tem uma",  "Branca", "Verde", null, "Esperando confirmação", null),

(6, "Broa de Fuba", "476.319.301-25", "+55 24 98824-2424", 55, 45, "Tá mais facil almoçar em paris do que na casa da tal de sogra", "Branca", "Azul", null, "Aguardando pagamento", null);

insert into caderninho values
(1, "Samukinha", "+55 31 99987-2719", 20, 20, "Nada é impossivel pra quem desiste", "Branca", "Verde",1 , 20),

(2, "Vigarista", "+55 11 98618-8845", 35, 40, "Nada é tão horrivel que não possa piorar!", "Cinza", "Preto",1 , 58),

(3, "Bolsonaro", "+55 18 99472-6533", 10, 10, "Talkey", "Branco", "Vermelho",1 , 90),

(4, "Zeca PaGodinho", "+55 41 98126-7410", 10, 20, "Seja o protagonista do seu fracasso", "Cinza", "Azul",1 , 76),
(5, "Mocreia", "+55 08 99161-9906", 100, 100, "O lado bom de ser feio é que sua namorada te ama de verdade agora o lado ruim é que você não tem uma",  "Branca", "Verde",1 , 53),
(6, "Broa de Fuba", "+55 24 98824-2424", 55, 45, "Tá mais facil almoçar em paris do que na casa da tal de sogra", "Branca", "Azul",1 , 80);


insert into cadastro values
("264.804.737-41", "Samukinha", "+55 31 99987-2719",1 ,1),
("123.45.789-00", "Vigarista", "+55 11 98618-8845",2 , 2),
("865.971.623-57", "Bolsonaro", "+55 18 99472-6533",3 ,3),
("512.741.202-96", "Zeca PaGodinho", "+55 41 98126-7410", 4, 4),
("789.210.374-30", "Mocreia", "+55 08 99161-9906",5 , 5),
("476.319.301-25", "Broa de Fuba", "+55 24 98824-2424",6 , 6);

/*Calculos*/

update caderninho
set area_1 = AlturaPlaca * LarguraPlaca
where id = 1;

update caderninho
set area_1 = AlturaPlaca * LarguraPlaca
where id = 2;

update caderninho
set area_1 = AlturaPlaca * LarguraPlaca
where id = 3;

update caderninho
set area_1 = AlturaPlaca * LarguraPlaca
where id = 4;

update caderninho
set area_1 = AlturaPlaca * LarguraPlaca
where id = 5;

update caderninho
set area_1 = AlturaPlaca * LarguraPlaca
where id = 6;


DELIMITER $$
create function Valor_mat(area_1 double)
	
returns double deterministic
begin 
	declare area0 double;
	set area0 = area_1 * 123.50;
    return area0;
end $$

delimiter $$
create function Valor_Des(Frase varchar(110))

returns double deterministic
begin 
	declare desenho double;
    set desenho = 0.41 * CHARACTER_LENGTH(Frase);
    return desenho;
end $$

delimiter $$
create procedure Custo_total()
begin
	declare total double;
    declare A int default 1;
    while A < 7 do
		set total = (select Valor_Des(Frase) from caderninho where caderninho.id = A) + (select Valor_mat(area_1) from caderninho where caderninho.id = A);
        
        update calculos
        set ValorPlaca = total where id1 = A;

        update reciboEmpresa
        set ValorPlaca1 = total where id2 = A;
        
		if total * 0.01 > 100 then
			set total = 100;
		end if;
        
        update caderninho
        set Valor_Sinal = total * 0.5
        where id = A;
        
        update reciboEmpresa
        set Valor_Sinal = total  * 0.5
        where id2 = A;
        
        set A = A + 1;
	end while;
end $$

call Custo_total();

create view segundaVia 
as select cadastro.cpf, cadastro.nome, cadastro.telefone, cadastro.id, reciboEmpresa.id2, reciboEmpresa.situacao from cadastro inner join reciboEmpresa on cadastro.id = reciboEmpresa.id2;

create view pesquisa_cliente 
as 
select caderninho.nome, reciboEmpresa.cpf,
reciboEmpresa.situacao, reciboEmpresa.Valor_Sinal
from caderninho inner join reciboEmpresa 
on caderninho.id = reciboEmpresa.id2; 


create view pesquisa
as
select caderninho.nome, reciboEmpresa.CPF, caderninho.telefone, reciboEmpresa.situacao from caderninho inner join reciboEmpresa on caderninho.id = reciboEmpresa.id2;

select * from pesquisa_cliente;

select * from reciboEmpresa;

select * from segundaVia;

select * from pesquisa where CPF = "264.804.737-41" and telefone = "+55 31 99987-2719";