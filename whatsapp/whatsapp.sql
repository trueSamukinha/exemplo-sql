drop database Whatsapp;
create database Whatsapp;
use Whatsapp;

create table Cadastros(
Id int not null primary key,
Nome varchar(15) not null,
Telefone varchar(15) not null,
Email varchar(40) not null,
senha varchar(30) not null,
Perfil varchar(20) not null
);

create table Operador(
Nomes varchar(15) not null primary key,
Opera varchar(15),
quantatendimentosdia int,
StatusPessoa varchar(20),
Visibilidade varchar(10) not null,
Telefone varchar(15),
DatadeCadastro date not null
);

create table Cliente(
nome varchar(15) not null,
telefone int not null,
StatusPessoa varchar(20) not null,
operacao varchar(20)
);

create table BOT(
Id int not null,
Mensagem varchar(100),
ticket varchar(20) not null primary key,
operacao varchar(20)
);

alter table BOT add foreign key (Id) references Cadastros(Id);

create table Atendimento(
Nomes varchar(15),
operacao varchar(20),
Datadoatendimento date,
quantatendimentosdia int,
ticket varchar(20),
quantmensagens int not null,
Statuschamada varchar(20) not null 
);

create table Finalização(
quantmensagens int,
ticket varchar(20),
quantatent int,
Datadeinicio date not null,
Datadetermino date,
Statuschamada varchar(20)
);

alter table Finalização add foreign key (ticket) references BOT(ticket);

create table Historico(
quantmensagens int,
ticket varchar(20),
quantatent int,
Datadeinicio date,
Datadetermino date,
Nome varchar(15),
operacao varchar(20)
);

create table Relatorio(
Nome varchar(15),
quantmensagens int,
quantatent int,
Datadeinicio date,
Datadetermino date
);

create table Analitico(
Nome varchar(15),
quantatent int,
ticket varchar(20),
quantmensagens int,
Datadeinicio date,
Datadetermino date
);

alter table Analitico add foreign key (ticket) references BOT(ticket);

Create table Sintetico(
Nome varchar(15),
quantatent int
);

/* 3 */

insert into Cadastros values 
(1234, "Felipão", 94968988, "felipeaugusto0810@gmail.com", "AmoSamukinha", "Administrador"),
(1579, "Camila", 986188855, "camilacamomila@gmail.com", "FelipeNaoMeAma","cliente"),
(9886, "Pedro",  94010109, "prf.aguiar@gmail.com", "AmooIgor", "operador"),
(6548, "Igor", 984374097, "igorrichard13@hotmail.com", "OdeiooPedro", "operador"),
(7235, "Samukinha", 999872719, "samuelhcmachado@gmail.com", "SoulindoDemais", "cliente"),
(8123, "Fabiane", 983244442, "fabianenaomebane@gmail.com", "SamuelehLindo","operador"),
(0462, "Cristiano", 997156134, "cristianotafumano@gmail.com", "FilosofodaNicotina", "cliente");

insert into Operador values
("Pedro", "Vendas", 0, "Online", "Visivel", 94010109, "2021-08-18"),
("Igor", "Suporte", 1, "Offline", "Invisivel", 984374097, "2021-07-05"),
("Fabiane", "Expedição", 1, "Online", "Invisivel",983244442, "2020-10-08");

insert into Cliente values
("Camila", 986188855, "Online", "Suporte"),
("Samukinha", 999872719, "Offline", null),
("Cristiano", 997156134, "Online", null);

insert into BOT values
(1579, "Olá, aguarde um momento para ser atendido(a) ;)", "$nxPTCNg6Ii!#-DRg6nb", "Suporte"),
(7235, "Quando quiser agendar um novo suporte é só me chamar ;)", "KXTOmi<c*r0u>O@QQ{x,", null),
(0462, "Fique a vontade para nos direcionar qualquer dúvida ;)", "|<y#*1>OZjqj7!zKKwn}", null);

insert into Atendimento values
("Pedro", "Vendas", "2021-07-20", 0, "GnLNbYi?whp8i|-n", 25, "Finalizado" ),
("Igor","Suporte", "2021-07-26", 1, "$nxPTCNg6Ii!#-DRg6nb", 0, "Em espera"),
("Fabiane", null, "2021-07-24", 1, "KXTOmi<c*r0u>O@QQ{x,", 4, "Pendente"),
("Pedro", null, "2021-07-24", 0, "|<y#*1>OZjqj7!zKKwn}", 38, "Finalizado");

insert into Finalização values
(0, "$nxPTCNg6Ii!#-DRg6nb", 2, "2021-07-26", null, "Em espera"),
(4, "KXTOmi<c*r0u>O@QQ{x,", 1, "2021-07-24", null, "Pendente");

insert into Historico values
(25, "GnLNbYi?whp8i|-n", 2, "2021-07-20", "2021-07-24", "Camila", "Vendas"),
(0, "$nxPTCNg6Ii!#-DRg6nb", 2, "2021-07-26", null, "Camila", "Suporte"),
(4, "KXTOmi<c*r0u>O@QQ{x,", 1, "2021-07-24", null, "Samukinha", null),
(38, "|<y#*1>OZjqj7!zKKwn}", 1, "2021-07-24", "2021-07-26", "Cristiano", null);

insert into Relatorio values
("Camila", 0, 2, "2021-07-26", null),
("Samukinha", 4, 1, "2021-07-24", null),
("Cristiano", 38, 1, "2021-07-24", "2021-07-26");

insert into Analitico values
("Camila", 2, "GnLNbYi?whp8i|-n", 25, "2021-07-20", "2021-07-24"),
("Camila", 2, "$nxPTCNg6Ii!#-DRg6nb", 0, "2021-07-26", null),
("Samukinha", 1, "KXTOmi<c*r0u>O@QQ{x,", 4, "2021-07-24", null),
("Cristiano", 1, "|<y#*1>OZjqj7!zKKwn}", 38, "2021-07-24", "2021-07-26");

insert into Sintetico values
("Camila", 2),
("Samukinha", 1),
("Cristiano", 1);

/* 4 */

select * from Operador where DatadeCadastro > 2021-07-15;

/* 5 */

select * from Cliente where telefone like "%55";

/* 6 */

select nome, quantmensagens from Historico group by nome order by quantmensagens;

/* 7  */

select quantatendimentosdia from Operador;

/* 8  */

select quantatendimentosdia from Atendimento where Datadoatendimento like "2021-07-15";

/* 9  */

select * from Atendimento where Statuschamada like "%Pendente%" or Statuschamada like "%Em espera%";

/* 10 */

select avg(quantatent) from Historico group by Datadetermino;

/* 11 */

select count(*) from Cliente; 

/* 12 */

select quantatent from Historico where quantmensagens > 5 group by Nome;

/* 13 */

select avg(quantmensagens) from Atendimento group by  Nomes having avg(quantmensagens) > 5;

/* 14 */ 

select nome, quantmensagens from Historico where quantmensagens = 25
union
select nome, quantatent from Historico where quantatent = 1;

select * from Historico inner join Atendimento on Historico.ticket = Atendimento.ticket;

select * from Historico right outer join Atendimento on Historico.ticket = Atendimento.ticket;

select * from Historico left outer join Atendimento on Historico.ticket = Atendimento.ticket;

select Nomes, operacao from Atendimento
where quantatent > (select avg(quantatent) from Atendimento);
